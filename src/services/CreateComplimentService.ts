import { getCustomRepository } from "typeorm"
import { ComplimentsRepositories } from "../repositories/ComplimentsRepositories"
import { UsersRepositories } from "../repositories/UsersRepositories";



interface IComplimentRequest {
    tag_id: string;
    user_sender: string;
    user_reciver: string;
    message: string;
}

class CreateComplimentService {

    async execute({ tag_id, user_sender, user_reciver, message }: IComplimentRequest) {
        const complimentsRepositories = getCustomRepository(ComplimentsRepositories);
        const usersRepositories = getCustomRepository(UsersRepositories);

        const userReciverExists = await usersRepositories.findOne(user_reciver);

        if (user_sender == user_reciver) {
            throw new Error("Incorrect user reciver!")
        }

        if (!userReciverExists) {
            throw new Error("User reciver does not exists!");
        }

        const compliment = complimentsRepositories.create({
            tag_id,
            user_sender,
            user_reciver,
            message
        });

        await complimentsRepositories.save(compliment);

        return compliment;

    }

}

export { CreateComplimentService }