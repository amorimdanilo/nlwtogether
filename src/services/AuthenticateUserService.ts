import { compare } from "bcryptjs";
import { getCustomRepository } from "typeorm";
import { UsersRepositories } from "../repositories/UsersRepositories";
import { sign } from "jsonwebtoken";



interface IAuthenticateRequest {
    email: string;
    password: string;
}

class AuthenticateUserService {

    async execute({ email, password }: IAuthenticateRequest) {
        const usersRepositories = getCustomRepository(UsersRepositories);
        // verificar se email existe
        const user = await usersRepositories.findOne({
            email
        });

        if (!user) {
            throw new Error("Email/Password incorrect")
        }
        // verificar se senha está correta
        const passwordHash = await compare(password, user.password);

        if (!passwordHash) {
            throw new Error("Email / Password incorrect");
        }
        // verificar token
        const token = sign({
            email: user.email
        }, "1f13fae9b210630718be6ffeec1d9fb9",
            {
                subject: user.id,
                expiresIn: "1d"
            }
        );
        return token;
    }
}

export { AuthenticateUserService }