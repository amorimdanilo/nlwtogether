import { getCustomRepository } from "typeorm";
import { TagsRepositories } from "../repositories/TagsRepositories";
// depois de colocar na entidade criar esse import abaixo no service
// com isso preciso da no return o classtoPlain
// criando assim a nameCustom e todas as outras que eu tiver
import { classToPlain } from "class-transformer";


class ListTagsService {
    //retorno de todas as tags de forma simples e eficaz 
    async execute() {
        const tagsRepositories = getCustomRepository(TagsRepositories);
        // forma de fazer 1
        const tags = await tagsRepositories.find();
        // outra forma de fazer forma2
        // let tags = await tagsRepositories.find();
        // tags = tags.map((tag) => ({
        //     ...tag,
        //     nameCustom: `#${tag.name}`
        // }));
        // existe outra forma que é usando uma biblioteca chamada class-transformer tem que ser instalada
        // onde será incluida o "#" direto 

        return classToPlain(tags);
    }

}

export { ListTagsService };