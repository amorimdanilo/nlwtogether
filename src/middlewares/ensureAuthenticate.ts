import { Request, Response, NextFunction } from "express";
import { verify } from "jsonwebtoken";

interface IPayload {
    sub: string;
}

export function ensureAuthenticate(
    request: Request,
    response: Response,
    next: NextFunction
) {
    //receber o token
    const authToken = request.headers.authorization;

    //validar se o token esta preenchido
    if (!authToken) {
        return response.status(401).end();
        // return response.status(401).json({ message: "Token missing" }); pode ser utilizado dessa forma
    }

    const [, token] = authToken.split(" ");
    try {
        //validar se token é valido
        const { sub } = verify(token, "1f13fae9b210630718be6ffeec1d9fb9") as IPayload;
        //request de onde vem o id do usuário
        request.user_id = sub;//Lembrando que preciso ir no tsconfig e incluir dentro da typeRoots incluir a rota da minha pasta @types que sobrescreve a outra

        return next();
    } catch (err) {
        return response.status(401).end();
    }

    //recuperar informaçõse do usuário



}