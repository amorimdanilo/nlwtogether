import { Router } from "express";
import { CreateUserController } from "./src/controllers/CreateUserController";
import { CreateTagController } from "./src/controllers/CreateTagController";
import { ensureAdmin } from "./src/middlewares/ensureAdmin";
import { AuthenticateUserController } from "./src/controllers/AuthenticateUserController";
import { CreateComplimentController } from "./src/controllers/CreateComplimentController";
import { ensureAuthenticate } from "./src/middlewares/ensureAuthenticate";
import { ListUserSendComplimentsController } from "./src/controllers/ListUserSendComplimentsController";
import { ListUserReciveComplimetsController } from "./src/controllers/ListUserReciveComplimentsController";
import { ListTagController } from "./src/controllers/ListTagController";
import { ListUserController } from "./src/controllers/ListUserController";


const router = Router();

const createUserController = new CreateUserController();
const createTagController = new CreateTagController();
const authenticateUserController = new AuthenticateUserController();
const createComplimentController = new CreateComplimentController();
//rota das listas
const listUserSendComplimentsController = new ListUserSendComplimentsController();
const listUserReciveComplimentsController = new ListUserReciveComplimetsController();
//rota de listar tags
const listTagController = new ListTagController();
//rota lista de usuarios
const listUserController = new ListUserController();

//subntende-se que o (request, response nao é necessário pois está no handle)
router.post("/users", createUserController.handle);
router.post("/tags", ensureAuthenticate, ensureAdmin, createTagController.handle);
router.post("/login", authenticateUserController.handle);
router.post("/compliment", ensureAuthenticate, createComplimentController.handle);
//rota para listar quem enviou e quem recebeu quais elogios
router.get("/users/compliments/send", ensureAuthenticate, listUserSendComplimentsController.handle);
router.get("/users/compliments/recive", ensureAuthenticate, listUserReciveComplimentsController.handle);
//rota para listar as tags
router.get("/tags", ensureAuthenticate, listTagController.handle);
//rota para listar usuários
router.get("/users", ensureAuthenticate, listUserController.handle);



export { router };